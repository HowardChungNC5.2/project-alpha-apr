from django.db import models
from django.contrib.auth.models import User
from projects.models import Project


# Create your models here.
class Task(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateTimeField()
    due_date = models.DateTimeField()
    in_progress = models.BooleanField(default=False)
    is_complete = models.BooleanField(default=False)
    estimate_cost = models.DecimalField(
        max_digits=6, decimal_places=2, null=True, blank=True
    )
    project = models.ForeignKey(
        Project,
        related_name="tasks",
        on_delete=models.CASCADE,
    )
    assignee = models.ForeignKey(
        User,
        related_name="tasks",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.name
