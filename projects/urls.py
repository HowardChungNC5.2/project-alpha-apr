from django.urls import path
from projects.views import list_projects, show_project, create_project, go_back

urlpatterns = [
    path("", list_projects, name="list_projects"),
    # see notes_features.txt for function explanation
    path("<int:id>/", show_project, name="show_project"),
    path("create/", create_project, name="create_project"),
    path("go_back/", go_back, name="go_back"),
]
